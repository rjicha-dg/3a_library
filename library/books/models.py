from django.db import models

class Book(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    year = models.SmallIntegerField(null=True, blank=True)

    def __str__(self):
        return self.title
